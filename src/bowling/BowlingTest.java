package bowling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BowlingTest {

    private Bowling game;

    @BeforeEach
    public void before() {
        this.game = new Bowling();
    }

    @Test
    public void constructs() {
        assertNotNull(game);
    }

    private void rollMany(int n, int pin) {
        for (int i = 0; i < n; i++) {
            game.roll(pin);
        }
    }

    @Test
    public void testOnlyGutter() {
        rollMany(20, 0);
        assertEquals(0, game.score());
    }

    @Test
    public void testOnes() {
        rollMany(20, 1);
        assertEquals(20, game.score());
    }

    @Test
    public void givenInvalidPinValues_whenRoll_scoreMinusOne() {
        game.roll(-3);
        assertEquals(-1, game.score());
    }

    @Test
    public void spare() {






    }

    // Werte über 9
    // der zweite Wurf ist in Summe mehr als 10
    // Insgesamt mehr als 20 roll-Aufrufe


}

