package bowling;

public class Bowling {

    private int sum = 0;

    public void roll(int pins) {
        sum += pins;
    }

    public int score() {
        return sum;
    }
}
