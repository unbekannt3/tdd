package linkedList;

public class LinkedListElement {
    public String value;
    public LinkedListElement tail;

    public LinkedListElement(String value) {
        this.value = value;
    }
}
