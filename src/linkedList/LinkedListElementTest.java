package linkedList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LinkedListElementTest {

    @Test
    public void can_be_const_with_val() {
        LinkedListElement ele=new LinkedListElement("awd");
        assertNotNull(ele);
        assertEquals("awd", ele.value);
    }

    @Test
    public void hasATail() {
        LinkedListElement ele = new LinkedListElement("awd");
        ele.tail = new LinkedListElement("asd");
        assertEquals("asd", ele.tail.value);
    }



}
