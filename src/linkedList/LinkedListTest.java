package linkedList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LinkedListTest {


    @Test
    public void canBeConstructed() {
        assertNotNull(new LinkedList());
    }

    @Test
    public void empty_List_Has_Size_Zero() {
        LinkedList list=new LinkedList();
        assertEquals(0, list.size());
    }

    @Test
    public void one_add_call_has_size_1() {
        LinkedList list=new LinkedList();
        list.add("sadsad");
        assertEquals(1, list.size());
    }

    @Test
    public void two_add_call_has_size_2() {
        LinkedList list=new LinkedList();
        list.add("sadsad");
        list.add("wqdqd");
        assertEquals(2, list.size());
    }

    @Test
    public void three_calls_has_size_3() {
        LinkedList list=new LinkedList();
        list.add("sadsad");
        list.add("wqdqd");
        list.add("wqdqd");

        assertEquals(3, list.size());
    }

    @Test
    public void fifteen_calls_has_size_15() {
        LinkedList list=new LinkedList();
        for(int i=0;i<15;i++) {
            list.add("value -->  " +i);
        }

        assertEquals(15, list.size());
    }

    @Test
    public void get_0() {
        LinkedList list = new LinkedList();
        list.add("ich bin 1");
        list.add("ich bin 2");
        list.add("ich bin 3");
        list.add("ich bin 4");

        assertEquals("ich bin 1", list.get(0));
    }

    @Test
    public void get_15() {
        LinkedList list = new LinkedList();
        for (int i = 1; i <= 15; i++) {
            list.add("ich bin " + i);
        }

        assertEquals("ich bin 15", list.get(15));
    }

}
