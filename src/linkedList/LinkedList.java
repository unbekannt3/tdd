package linkedList;

public class LinkedList {

    private LinkedListElement anchor;

    public void add(String value) {
        if(this.anchor == null) {
            this.anchor = new LinkedListElement(value);
        } else {
            LinkedListElement currEle = anchor;
            while(currEle.tail != null) {
                currEle = currEle.tail;
            }
            currEle.tail = new LinkedListElement(value);
        }
    }

    public int size() {
        if(this.anchor == null) {
            return 0;
        }
        LinkedListElement currentEle = this.anchor;
        int counter = 1;
        while(currentEle.tail != null) {
            currentEle = currentEle.tail;
            counter++;
        }
        return counter;
    }

    public String get(int index) {
        if (index == 0) {
            return this.anchor.value;
        } else {

            LinkedListElement currEle = this.anchor;
            int i = 0;
            while(currEle.tail != null && i < index) {
                i++;
                currEle = currEle.tail;
            }
            return currEle.value;
        }
    }

}
