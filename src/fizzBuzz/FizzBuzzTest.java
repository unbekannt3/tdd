package fizzBuzz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class FizzBuzzTest {

    // 1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16

    @Test
    public void canBeConstructed() {
        assertNotNull(new FizzBuzzCalculator());
    }

    @Test
    public void testOne() {

        FizzBuzzCalculator calc = new FizzBuzzCalculator();
        assertEquals("1", calc.fizzBuzz(1));
    }

    @Test
    public void testThree() {

        FizzBuzzCalculator calc = new FizzBuzzCalculator();
        assertEquals("Fizz", calc.fizzBuzz(3));
    }

    @Test
    public void testFive() {

        FizzBuzzCalculator calc = new FizzBuzzCalculator();
        assertEquals("Buzz", calc.fizzBuzz(5));
    }

    @Test
    public void testFifteen() {

        FizzBuzzCalculator calc = new FizzBuzzCalculator();
        assertEquals("FizzBuzz", calc.fizzBuzz(15));
    }

}
