package fizzBuzz;

public class FizzBuzzCalculator {

    public String fizzBuzz(int figure) {

        if (figure % 3 == 0 && figure % 5 == 0) {
            return "FizzBuzz";
        } else if(figure % 3 == 0) {
            return "Fizz";
        } else if (figure % 5 == 0){
            return "Buzz";
        } else {
            return figure + "";
        }



    }

}
