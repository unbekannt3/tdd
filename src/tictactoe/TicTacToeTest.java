package tictactoe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TicTacToeTest {

    private TicTacToe game;

    @BeforeEach
    public void initiate() {
        game = new TicTacToe();
    }

    @Test
    public void testXOutOfBounds() {
        assertThrows(RuntimeException.class, () -> game.play(-1, 1));
        assertThrows(RuntimeException.class, () -> game.play(3,1));
    }

    @Test
    public void testXInBounds() {
        assertDoesNotThrow(() -> game.play(1,2));
    }

    @Test
    public void testOccupiedField() {
        game.play(1,1);
        assertThrows(RuntimeException.class, () -> game.play(1,1));
    }

    @Test
    public void testNextPlayerX() {
        assertEquals("X", game.nextPlayer());
    }

    @Test
    public void textNextPlayerO() {
        game.play(1,1);
        assertEquals("O", game.nextPlayer());
    }

    @Test
    public void testNoWinner() {
        assertEquals("No winner", game.play(1,1));
    }

    @Test
    public void testWinnerOHorizontal() {
        game.play(1,0);
        game.play(0,0);
        game.play(1,2);
        game.play(0,1);
        game.play(2,0);
        assertEquals("Winner O", game.play(0,2));
    }

    @Test
    public void testWinnerDiagonal1() {
        game.play(0,0);
        game.play(0,1);

        game.play(1,1);
        game.play(0,2);

        assertEquals("Winner X", game.play(2,2));
    }

    @Test
    public void testWinnerDiagonal2() {
        game.play(0,1);
        game.play(0,2);

        game.play(0,0);
        game.play(1,1);

        game.play(2,2);

        assertEquals("Winner O", game.play(2,0));
    }

    @Test
    public void testTieGame() {
        game.play(0,0);
        game.play(0,1);

        game.play(0,2);
        game.play(1,2);

        game.play(1,0);
        game.play(2,0);

        game.play(1,1);
        game.play(2,2);

        assertEquals("Tie Game", game.play(2,1));

    }
}
