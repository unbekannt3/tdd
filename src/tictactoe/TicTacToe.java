package tictactoe;

import java.util.Arrays;

public class TicTacToe {

    private String[][] field = new String[3][3];
    private boolean playerXactive = true;

    public String play(int x, int y) {

        if (x < 0 || x > 2 || y < 0 || y > 2) {

            throw new RuntimeException("Out of Bounds");
        }

        if (field[x][y] != null) {
            throw new RuntimeException("Field already occupied");
        } else {
            // System.out.println("player x: " + playerXactive);

            if (playerXactive) {
                field[x][y] = "X";
            } else {
                field[x][y] = "O";
            }

            boolean[] winCheck = checkForWinner(nextPlayer());

            // Debug
            // System.out.println("Gewonnen: " + winCheck[0] + "; Unentschieden: " + winCheck[1]);

            if (winCheck[0]) {
                return "Winner " + nextPlayer();
            }
            if (winCheck[1]) {
                return "Tie Game";
            }

            playerXactive = !playerXactive;
            return "No winner";

        }

    }

    private boolean[] checkForWinner(String player) {

        // System.out.println(Arrays.deepToString(field).replace("], ", "]\n"));

        boolean gewonnen = false;
        boolean unentschieden = false;

        // Zeilen/Spalten
        if (field[0][0] == player && field[0][1] == player && field[0][2] == player ||
                field[1][0] == player && field[1][1] == player && field[1][2] == player ||
                field[2][0] == player && field[2][1] == player && field[2][2] == player ||
                field[0][0] == player && field[1][0] == player && field[2][0] == player ||
                field[0][1] == player && field[1][1] == player && field[2][1] == player ||
                field[0][2] == player && field[1][2] == player && field[2][2] == player)
        {
            gewonnen = true;
        }

        // Diagonal
        if (field[0][0] == player && field[1][1] == player && field[2][2] == player)  {
            gewonnen = true;
        }

        if (field[2][0] == player && field[1][1] == player && field[0][2] == player) {
            gewonnen = true;
        }

        // Unentschieden / Tie Game
        if (field[0][0] != null && field[0][1] != null && field[0][2] != null &&
                field[1][0] != null && field[1][1] != null && field[1][2] != null &&
                field[2][0] != null && field[2][1] != null && field[2][2] != null) {
            unentschieden = true;
        }


        return new boolean[] { gewonnen, unentschieden };
    }

    public String nextPlayer() {

        if (playerXactive) {
            return "X";
        } else {
            return "O";
        }

    }

}
