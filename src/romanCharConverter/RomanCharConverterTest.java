package romanCharConverter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RomanCharConverterTest {

    @Test
    public void testOne() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("I", conv.convert(1));
    }

    @Test
    public void testTwo() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("II", conv.convert(2));
    }

    @Test
    public void test333() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCXXXIII", conv.convert(333));
    }

    @Test
    public void test334() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCXXXIV", conv.convert(334));
    }

    @Test
    public void test344() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCXLIV", conv.convert(344));
    }

    @Test
    public void test394() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCXCIV", conv.convert(394));
    }

    @Test
    public void test396() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCXCVI", conv.convert(396));
    }

    @Test
    public void test354() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCLIV", conv.convert(354));
    }

    @Test
    public void test359() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("CCCLIX", conv.convert(359));
    }

    @Test
    public void test500() {
        RomanCharConverter conv = new RomanCharConverter();
        assertEquals("D", conv.convert(500));
    }

}
